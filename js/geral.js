$(function(){

	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
		dots: true,
		loop: true,
		lazyLoad: true,
		mouseDrag:true,
		touchDrag  : false,
		autoplay:true,
		autoplayTimeout:10000,
		autoplayHoverPause:true,
		smartSpeed: 450,	
		animateOut: 'fadeOut',
	});
	var carrosselDepoimentos = $("#carrosselDepoimentos").data('owlCarousel');
	$('.esquerdaCarrossel').click(function(){ carrosselDepoimentos.prev(); });
	$('.direitaCarrossel').click(function(){ carrosselDepoimentos.next(); });

	//CARROSSEL DE DESTAQUE
	$("#carrosselServico").owlCarousel({
		items : 4,
		dots: true,
		loop: true,
		lazyLoad: true,
		mouseDrag:true,
		touchDrag: false,
		autoplay:true,
		autoplayTimeout:10000,
		autoplayHoverPause:true,
		smartSpeed: 450,	
		animateOut: 'fadeOut',
		responsiveClass:true,			    
		responsive:{
			320:{
				items:1
			},
			400:{
				items:2
			},
			600:{
				items:3
			},
			991:{
				items:4
			},
			1024:{
				items:4
			},
		}
	});

	//CARROSSEL DE DESTAQUE
	$("#carrosselCalendario").owlCarousel({
		items : 1,
		dots: true,
		loop: false,
		lazyLoad: true,
		mouseDrag:true,
		touchDrag  : false,
		smartSpeed: 450,	
		animateOut: 'fadeOut',
	});
	var carrosselCalendario = $("#carrosselCalendario").data('owlCarousel');
	$('.esquerdacarrosselCalendario').click(function(){ carrosselCalendario.prev(); });
	$('.direitacarrosselCalendario').click(function(){ carrosselCalendario.next(); });
	
	$(".pg-agenda .carrosselCalendario .item ul li span").click(function(e){
		$(".pg-agenda .carrosselCalendario .item ul li span").removeClass('ativo');
		$(this).addClass('ativo');
		console.log($(this).attr('data-dia'));
	});


	$('a.scrollTop').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}

	});

	$('header.topo .btnAbrirMenuMobile').click(function(e){
		$('header.topo .menuMobile').addClass('abrirMenuMobile');
	});

	$('header.topo .btnFecharMenuMobile').click(function(e){
		$('header.topo .menuMobile').removeClass('abrirMenuMobile');
	});
	
});